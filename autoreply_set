#!/bin/bash

script=$(basename ${0})
sieve_file=".dovecot.sieve"
vacation=0
leaving=0
disable=0
script_mode=0
sched_start=0
sched_end=0
message_only=0
activate=1
edit_file=1
list=0
list_scheduled=0
queue="v"

message(){ echo "${script}:" "${1}"; } # TODO: use these functions more.

error(){ echo "${script}: error ->" "${1}" >&2; }

fatal(){ error "${1}" >&2; exit 1; }

usage(){
	cat <<-EOD
	Usage: ${script} [OPTION...] <user>
	
	    -h,  show this help
	    -v,  set the vacation message (default)
	    -x,  set the leaving message
	    -d,  disable the autoreply message now
	    -s YYYY-MM-DD, auto-start the autoreply at this date (start)
	    -e YYYY-MM-DD, auto-disable the autoreply at this date (end)
	    -l,  print the list of active autoreplies
	    -L,  print the list of scheduled jobs
	    -S,  run in script mode -> don't edit message file with text editor
	    -M,  only edit or prepare the message file
	EOD
}

generate_message(){
	cat <<-EOD
	require "vacation";
	vacation :days 1 :addresses ["${user_mail}"]

	$(cat ${msg_file_global})
	EOD
}

print_scheduled(){
	local -a v_jobs

	while read -r job; do
    	v_jobs+=("${job}")
	done < <(at -l -q ${queue})

	for job in "${v_jobs[@]}"
	do
		id=$(cut -f1 <<< "${job}")
		cmd=$(at -c ${id} | grep -E "^autoreply_set")
		echo "${job}" " | " "${cmd}"
	done | sort -n
}

show_type(){
	local file="${1}"
	if [[ $(stat -c '%h' ${file}) > 1 ]]; then
		local inode=$(stat -c '%i' ${file})
		if [[ -f "$(dirname ${file})/.vacation.sieve" && \
			( ${inode} == $(stat -c '%i' "$(dirname ${file})/.vacation.sieve") ) ]]; then
			echo -n vacation
		elif [[ -f "$(dirname ${file})/.leaving.sieve" && \
			( ${inode} == $(stat -c '%i' "$(dirname ${file})/.leaving.sieve") ) ]]; then
			echo -n leaving
		fi
	fi
}

print_active(){
	while read -r file
	do
		dir=$(dirname ${file})
		show_type $file
		echo -n ":"
		echo " ${dir##*/} "
	done < <(find /home/* -maxdepth 1 -name "${sieve_file}") | sort
}

yes_no_fatal(){
        message "${1} y/n"
        read -n 1 -p "?" -t 60 answer; echo ""
        [[ $answer != "y" ]] && fatal "aborting script"
}

OPTIND=1
while getopts "hxvdSMlLs:e:" opt; do
	case "${opt}" in
		h)
			usage
			exit 0
			;;
		x)
			leaving=1
			;;
		v)
			vacation=1
			;;
		s)
			sched_start=1
			sched_start_date=${OPTARG}
			activate=0
			;;
		e)
			sched_end=1
			sched_end_date=${OPTARG}
			activate=0
			;;
		d)
			disable=1
			;;
		S)
			script_mode=1 # Do not edit the message file with nano
			edit_file=0
			;;
		M)
			message_only=1
			activate=0 # Only edit the message file with nano
			;;
		l)
			list=1
			list_active=1
			;;
		L)
			list=1
			list_scheduled=1
			;;
		'?')
			usage >&2
			exit 1
			;;
	esac
done
shift "$((OPTIND-1))" # Shift off the options and optional --.

(( (! ${list}) && (${#} != 1) )) && { usage; exit 1; }

#############
# Check input

# Lx Lv Ld LS lM Ls Le, xv xd xS xM xs xe, vd vS vM vs ve, dS dM ds de, SM Ss Se, Ms Me, se
# possible:
# -x|v 	& -S|M|d|s|e
# -S	& -s|e
# -d	& -S
# -s	& -e
# not possible:
# -x 	& -v
# -M	& -S|d|s|e
# -d	& -s|e
# -L	& -v|x|d|M|S|s|e

(( ${vacation} && ${leaving} )) && \
	{ echo "${script} error: You cannot combine option -v (vacation) with -x (leaving)"; exit 1; }

(( ${message_only} )) && (( ${script_mode} || ${disable} || ${sched_start} || ${sched_end} )) && \
	{ echo "${script} error: You cannot combine option -M (message_only) with -S, -s, -e, or -d"; exit 1; }

(( ${disable} )) && (( ${sched_start} || ${sched_end} )) && \
	{ echo "${script} error: You cannot combine option -d (disable) with -s, or -e"; exit 1; }

(( ${list} )) && (( ${vacation} || ${leaving} || ${sched_start} || ${sched_end} || ${disable} || ${message_only} || ${script_mode} )) && \
	{ echo "${script} error: You cannot combine options -l or -L (list) with any of the other options"; exit 1; }

# Set vacation on, even if the user hasn't specified the -v option.
if (( ! ${leaving} )); then vacation=1; fi

# Don't edit file if only scheduling autoreply end
# We do this before checking sched_start_date, because sched_start might be turned off there,
# but in that case we DO want to have edit_file on!

if (( ${sched_end} && ! ${sched_start} )); then edit_file=0; fi

# We check the sched_x_dates early to avoid surprises like: autoreply_set -eM lieven
# This would take M as the $OPTARG of -e
#
# If start-date is today but in the past, activate the message now.

if (( ${sched_start} )); then
	if ! [[ ${sched_start_date} =~ ^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}$ ]]; then
		echo "${script} error: Autoreply start date should be like YYYY-MM-DD"
		exit 1
	fi
	if ! sched_start_secs=$(date -d "${sched_start_date}" +%s); then
		exit 1
	fi
	if (( ${sched_start_secs} >= $(date -d "today 00:00" +%s) && \
		  ${sched_start_secs} <= $(date +%s) )); then
		activate=1
		sched_start=0 # (edit_file only depends on script mode)
	fi
	if (( ${sched_start_secs} < $(date -d "today 00:00" +%s) )); then
		echo "${script} error: Autoreply start date is in the past"
		exit 1
	fi
fi

# TODO:error message when date is wrong, like 29 feb
if (( ${sched_end} )); then
	if ! [[ ${sched_end_date} =~ ^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}$ ]]; then
		echo "${script} error: Autoreply end date should be like YYYY-MM-DD"
		exit 1
	fi
	if ! date -d "${sched_end_date}" &>/dev/null ; then
		exit 1
	fi
fi

# Check that the end date is later than the start date
(( ${sched_start} && ${sched_end} )) && (( $(date -d ${sched_start_date} +%s) >= $(date -d ${sched_end_date} +%s) )) && \
		{ echo "${script} error: Start date is greater or equal than end date"; exit 1; }

#############################################################
# Check if user is root

if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "${script}: Please run this script as root or with sudo!"
    exit 1
fi

############################################################
# Set a couple of variables that depend on the options given

if (( ${leaving} )); then
	msg_file_global="/my/path/templates/autoreply_set/leaving_message.txt"
	msg_file=".leaving.sieve"
else
	msg_file_global="/my/path/templates/autoreply_set/vacation_message.txt"
	msg_file=".vacation.sieve"
fi
####################
# List jobs and exit

if (( ${list} )); then

	if (( ${list_scheduled} )); then
		print_scheduled
	fi

	if (( ${list_active} )); then
		print_active
	fi
	exit 0
fi

#############################
# check the username and home

user="${1}"
pw_entry=$(getent passwd "${user}")
(( ${?} != 0 )) && { echo "${script} error: User \"${user}\" not found"; exit 1; }

user_home=$(echo ${pw_entry} | cut -d ":" -f6)
[[ ${user_home} =~ "/home/${user}" ]] || \
		{ echo "${script} error: User \"${user}\" doesn't have the expected home directory."; exit 1; }

#################################
# Do everything from ${user_home}

cd "${user_home}"

###################
# Disable and exit.

if (( ${disable} )); then
	echo "${script}: Disabling the autoreply message for user => ${user}"
	rm -f "${sieve_file}"
	exit 0
fi

#####################
# Get email address

ldapcompare -xz "uid=${user},ou=People,dc=example,dc=com" mail:
ret=${?}

if [[ ${ret} -eq 16 || ${ret} -eq 6 ]]; then  # undefined or true (empty)
	error "couldn't get email adress from user ${user}"
else
	user_mail=$(ldapsearch -xLLL -b "uid=${user},ou=People,dc=example,dc=com" mail | grep 'mail:' | sed -r 's/mail: //')
fi

######################################
# Prepare the 'vacation' message file

if (( ${edit_file} )); then

	echo -e "${script}: Generating the sieve file\t=> \"${user_home}/${msg_file}\""

	if [[ ! -f ${msg_file} ]]; then # We need to run this test as root!
		generate_message | sudo -u ${user} tee "${msg_file}" > /dev/null
	fi

	sudo -u ${user} nano -t "${msg_file}"
	(( ${message_only} )) || yes_no_fatal "Message file has been saved. Do you want to continue?"

else

	if (( ${script_mode} )) && [[ ! -f ${msg_file} ]]; then
		echo "${script} error: When you run in script_mode, you have to prepare the message file first with option -M"
		exit 1
	fi

fi

######################################
# Activate the autoreply

if (( ${activate} )); then

	echo -e "${script}: Creating link to sieve file\t=> \"${user_home}/${sieve_file}\""
	sudo -u ${user} ln -f "${msg_file}" "${sieve_file}"

	echo "${script}: Autoreply is set!"
fi

#################
# Schedule start

if (( ${sched_start} )); then

	if (( ${leaving} )); then opts="x"; else opts="v"; fi
	opts="-S${opts}"
	
	echo "${script}: Scheduling \"at\" job to start the autoreply automatically at => ${sched_start_date} 00:00"

	at -q ${queue} -M 00:00 ${sched_start_date} <<-END
		{ echo "autoreply_set ${opts} ${user}"
		autoreply_set ${opts} ${user}; } | mailx -s "autoreply log" root@example.com 2>&1
	END

	if (( ${?} != 0 )); then
		echo "${script} error: There was a problem scheduling the automatic enabling of the autoreply message"
		exit 1
	fi
fi

###############
# Schedule end

if (( ${sched_end} )); then

	echo "${script}: Scheduling \"at\" job to stop the autoreply automatically at => ${sched_end_date} 24:00"

	at -q ${queue} -M 24:00 ${sched_end_date} <<-END
		{ echo "autoreply_set -d ${user}"
		autoreply_set -d ${user}; } | mailx -s "autoreply log" root@example.com 2>&1
	END

	if (( ${?} != 0 )); then
		echo "${script} error: There was a problem scheduling the automatic disabling of the autoreply message"
		exit 1
	fi
fi

exit 0
